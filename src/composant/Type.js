import React, { useEffect, useState } from 'react';
//import { useSelector } from 'react-redux';
import Navigation from '../navbar/Navigation';
import axios from 'axios';
import { Table, Container } from 'react-bootstrap';

const Type = () => {

    //const types = useSelector((state) => state.types)
    const [typeList, setTypeList] = useState([])

    useEffect(() => {
        axios.get('http://localhost:3001/api/type').then(response => {
            console.log(response)
            setTypeList(response.data)
        }).catch(err => console.log(err))
    }, [])

    return (

        <div>
            <Navigation />
            <br />
            <u><h1>Type of user</h1></u>
            <Container>
                <Table striped bordered hover variant="light">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Libelle</th>
                        </tr>
                    </thead>
                    <tbody>
                        {typeList && typeList.map((value, key) =>
                        (   <tr key={key}>
                                <td>{value.id}</td>
                                <td>{value.libelle}</td>
                            </tr>))}
                    </tbody>
                </Table>
            </Container>



        </div>
    );
};

export default Type;