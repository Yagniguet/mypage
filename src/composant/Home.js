import React from 'react';
import Navigation from '../navbar/Navigation';
import { Carousel, Card, Button, CardGroup } from 'react-bootstrap';

const Home = () => {
    return (
        <div>
            <Navigation />
            <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/pro1.png"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/devpro1.png"
                        alt="Second slide"
                    />

                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/dev3.png"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            <h1 >CARDS</h1>
            <CardGroup>
                <Card >
                    <Card.Img 
                    variant="top" 
                    src="/logo192.png" 
                    />
                    <Card.Body>
                        <Card.Title>Card 1</Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        </Card.Text>
                    </Card.Body>
                    <Button variant="dark">Go somewhere</Button>
                </Card>
                <Card>
                    <Card.Img variant="top" src="/logo192.png" />
                    <Card.Body>
                        <Card.Title>Card 2</Card.Title>
                        <Card.Text>
                            This card has supporting text below as a natural lead-in to additional content.
                        </Card.Text>
                    </Card.Body>
                    <Button variant="success">Go somewhere</Button>
                </Card>
                <Card>
                    <Card.Img variant="top" src="/logo192.png" />
                    <Card.Body>
                        <Card.Title>Card 3</Card.Title>
                        <Card.Text>
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        </Card.Text>
                    </Card.Body>
                    <Button variant="primary">Go somewhere</Button>
                </Card>
            </CardGroup>  
        </div>
    );
};

export default Home;