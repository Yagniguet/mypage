import React, { useState } from 'react';
import Navigation from '../../navbar/Navigation';
import { Form, Button, Container } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
// import { addType } from '../../counter/TypeCounter';
import axios from 'axios';

const TypeForm = () => {

    const [libelle, setLibelle] = useState()
    const dispatch = useDispatch()

    const handleSubmit = (event) => {

        event.preventDefault();
        axios.post('http://localhost:3001/api/types', {libelle:libelle})
        .then(response =>{
            console.log(response)
            }).catch(err => console.log(err))
        // setLibelle((event.target.value));
        // dispatch(addType(libelle))
    }


    return (
        <div>
            <Navigation />
            <br />
            <u><h1>User type form</h1></u>
            <br />
            <h5>Please enter the type of user</h5>
            <br />
            <Form onSubmit={handleSubmit}>
                <Container>
                    <Form.Group className="mb-3" controlId="formBasicText">
                        <Form.Control
                            type="text"
                            placeholder="Enter the type of user"
                            value={libelle}
                            onChange={(event) => setLibelle(event.target.value)}
                        />
                    </Form.Group>
                </Container>
                <Button
                    variant="primary"
                    type="submit"
                    style={{
                        marginTop: '15px'
                    }}
                >
                    Submit
                </Button>
            </Form>
        </div>
    );
};

export default TypeForm;