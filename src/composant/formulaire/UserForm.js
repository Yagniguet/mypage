import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Form, Row, Col, Button, Container } from 'react-bootstrap';
import Navigation from '../../navbar/Navigation';
import { addUser } from '../../counter/UserCounter';

const UserForm = () => {
    const [firstname, setFirstname] = useState();
    const [lastname, setLastname] = useState();
    const [username, setUsername] = useState();
    const [city, setCity] = useState();
    const [type, setType] = useState();
    const [address, setAddress] = useState();

    const dispatch = useDispatch();

    const handleSubmit = (event) => {
        event.preventDefault();

        setFirstname((event.target.value))
        setLastname((event.target.value))
        setUsername((event.target.value))
        setCity((event.target.value))
        setType((event.target.value))
        setAddress((event.target.value))

        const data = { firstname, lastname, username, city, type, address };
        console.log(data);

        dispatch(addUser(data));

    };
    return (
        <div>
            <Navigation />
            <br />
            <u><h1>User form</h1></u>
            <br />
            <Form onSubmit={handleSubmit}>
                <Container>
                    <Row className="mb-3">
                        <Form.Group as={Col} md="4" controlId="validationCustom01">
                            <Form.Label >First name</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="First name"
                                value={firstname}
                                onChange={(event) => setFirstname(event.target.value)}
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationCustom02">
                            <Form.Label >Last name</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Last name"
                                value={lastname}
                                onChange={(event) => setLastname(event.target.value)}
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Username"
                                aria-describedby="inputGroupPrepend"
                                value={username}
                                required
                                onChange={(event) => setUsername(event.target.value)}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please choose a username.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Row>
                </Container>
                <Container>
                    <Row className="mb-3">
                        <Form.Group as={Col} md="6" controlId="validationCustom03">
                            <Form.Label >City</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="City"
                                required
                                value={city}
                                onChange={(event) => setCity(event.target.value)} />
                            <Form.Control.Feedback type="invalid">
                                Please provide a valid city.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="3" controlId="validationCustom04">
                            <Form.Label >Type</Form.Label>
                            <Form.Select
                                aria-label="Default select example"
                                onChange={(event) => setType(event.target.value)}
                                value={type}
                            >
                                <option></option>
                                <option value="1">Admin</option>
                                <option value="2">Root</option>
                                <option value="3">Inviter</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group as={Col} md="3" controlId="validationCustom05">
                            <Form.Label>Address</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Address"
                                required
                                value={address}
                                onChange={(event) => setAddress(event.target.value)} />
                            <Form.Control.Feedback type="invalid">
                                Please provide a valid address.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Row>
                </Container>
                <Button type="submit" >Submit </Button>
            </Form>
        </div>
    );
};

export default UserForm;