import React from 'react';
import { useSelector } from 'react-redux';
import Navigation from '../navbar/Navigation';
import {Table, Container} from 'react-bootstrap';

const User = () => {
    const users = useSelector((state) => state.users);


    return (

        <div>
            <Navigation />
            <br />
            <u><h1>Users list</h1></u>
            <br/>
            {
                    users?.users && users.users.map((users,key)=> (
            <div key={key}>
                <Container>
                <Table striped bordered hover variant="light">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>City</th>
                            <th>Type</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {users.firstname} </td>
                            <td> {users.lastname} </td>
                            <td> {users.usersname} </td>
                            <td> {users.city} </td>
                            <td> {users.type} </td>
                            <td> {users.address} </td>
                        </tr>
                    </tbody>
                </Table>
                </Container>
            </div>
                    ))
                } 
            
        </div>
    );
};

export default User;