import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Container, Nav, Form, Button } from 'react-bootstrap';
import "../style/navigation.css"


const Navigation = () => {
    return (
        <div className=''>
            <Navbar bg="dark" expand="lg" variant="dark">
                <Container>
                    <Navbar.Brand href="#home">
                        <img
                            alt=""
                            src="/logo192.png"
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Link className='liens' to="/">Home</Link>
                            <Link className='liens' to="/list">List user</Link>
                            <Link className='liens' to="/type">List type</Link>
                            <Link className='liens' to="/user-form">User form</Link>
                            <Link className='liens' to="/type-form">Type form</Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Form className="d-flex">
                        <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-primary">Search</Button>
                    </Form>
                </Container>
            </Navbar>
        </div>
    );
};

export default Navigation;