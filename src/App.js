import './App.css';
import { Routes, Route } from'react-router-dom';
import User from './composant/User';
import Type from './composant/Type';
import Home from './composant/Home';
import TypeForm from './composant/formulaire/TypeForm';
import UserFormm from './composant/formulaire/UserForm';

function App() {
  return (
    <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/list" element={<User />} />
          <Route path="/type" element={<Type/>}/>
          <Route path="/type-form" element={<TypeForm/>}/>
          <Route path="/user-form" element={<UserFormm/>}/>
          <Route path="*" element={<Home />} />
        </Routes>
    </div>
  );
}

export default App;
