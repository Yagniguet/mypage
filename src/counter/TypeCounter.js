import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  types : []
}
export const TypeCounter = createSlice({
  name: 'types',
  initialState,
  reducers: {
   addType : (state, action) => {
    state.types = [...state.types, action.payload];
   }
  }
})

export const { addType } = TypeCounter.actions;

export default TypeCounter.reducer;