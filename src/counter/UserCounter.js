import { createSlice } from '@reduxjs/toolkit'

const initialState = {
   users: []
}
export const UserCounter = createSlice({
  name: 'users',
  initialState,
  reducers: {
    addUser: (state, action) => {
      state.users = [...state.users, action.payload];
    }
  }
})
export const { addUser } = UserCounter.actions;

export default UserCounter.reducer;