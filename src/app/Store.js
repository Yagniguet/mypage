import { configureStore } from '@reduxjs/toolkit'
import UserCounter from '../counter/UserCounter'
import TypeCounter from '../counter/TypeCounter'

export const store = configureStore({
  reducer: {
    users : UserCounter,
    types : TypeCounter
  }
})